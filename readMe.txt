#banque-seti-fayaz

Etape 1:
- cr�er une base de donn�e "banque" dans pgAdmin 4;
- copier et coller les requ�tes toutes les requ�tes du fichier bdd.txt dans query tools de pgAdmin 4
- creer un utlisateur : fayaz-seti (mot de passe : fayaz-seti)

Etape 2:
- se connecter dans la base de donn�e "banque" dans DBeaver
- creer une nouvelle connection - Postgresql (database :banque)
- cr�e une banque avec une requ�te "insert into banque values (1, 'banque CDA') dans la table banque;
- cr�e 3 role avec une requ�te "insert into role values (1, 'admin'), (2, 'conseiller'), (3,'client) dans la table role;
- cr�e 3 type de compte avec une requ�te "insert into typecompte (1, 'courant'), (2, 'livreta'), (3, 'pel') dans la table typecompte;

Etape 3:
- ouvrir le projet avec netbeans ou eclipse
- lancer le projet � partir du fichier cr�ationLoginAdmin.java
- se connecter avec les logins suivant "admin" mot de passe "admin"

