package dto;

import servicebdd.BddQuerie;

public class Dto {
	
	public void dtoInsertAgence(String codeAgence, String nomAgenceSaisi, String adresseAgenceSaisi) {
		BddQuerie query = new BddQuerie();
		query.insertAgence(codeAgence, nomAgenceSaisi, adresseAgenceSaisi);
	}
	
	public void dtoInsertClient(String idClient, String nomClientSaisi, String prenomClientSaisi, String dateSaisi, String codeAgence, String adresseClientSaisi) {
		BddQuerie query = new BddQuerie();
		query.insertClient(idClient, nomClientSaisi, prenomClientSaisi, dateSaisi, codeAgence, adresseClientSaisi);
	}
	
	public void dtoInsertCompte(String num_compte, float solde, boolean decouvert, String id_client, int selected) {
		BddQuerie query = new BddQuerie();
		query.insertCompte(num_compte, solde, decouvert, id_client, selected);
	}
	
	public String [] dtoSelectAgence() {
		
		BddQuerie query = new BddQuerie();
		String[] tab = query.affichageListeAgence();
		
		return tab;
	}
        
        public String dtoGetCodeAgence(String nom){
            BddQuerie query = new BddQuerie();
		String codeAgence = query.getCodeAgence(nom);
		
		return codeAgence;
        }
        
        public String[] dtoGetInfosClient(String id_client){
            BddQuerie query = new BddQuerie();
		String[] tab = query.getInfosClient(id_client);
		
		return tab;
        }
}
