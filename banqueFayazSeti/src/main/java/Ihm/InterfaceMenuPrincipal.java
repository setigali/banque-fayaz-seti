/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ihm;

import javax.swing.JOptionPane;

/**
 *
 * @author setia
 */
public class InterfaceMenuPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form InterfaceMenuPrincipal
     */
    public InterfaceMenuPrincipal() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        bouttonCreerAgence = new javax.swing.JButton();
        bouttonCreerClient = new javax.swing.JButton();
        bouttonCreerCompteBancaire = new javax.swing.JButton();
        rechercherCompte = new javax.swing.JButton();
        rechercherClient = new javax.swing.JButton();
        afficherInfoClient = new javax.swing.JButton();
        imprimerInfoClient = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        modifierInfoClient = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        bouttonCreerAgence.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        bouttonCreerAgence.setText("Créer une agence");
        bouttonCreerAgence.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bouttonCreerAgenceMouseClicked(evt);
            }
        });

        bouttonCreerClient.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        bouttonCreerClient.setText("Créer un client");
        bouttonCreerClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bouttonCreerClientActionPerformed(evt);
            }
        });

        bouttonCreerCompteBancaire.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        bouttonCreerCompteBancaire.setText("Créer un compte bancaire");
        bouttonCreerCompteBancaire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bouttonCreerCompteBancaireActionPerformed(evt);
            }
        });

        rechercherCompte.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        rechercherCompte.setText("Rechercher un compte");
        rechercherCompte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rechercherCompteActionPerformed(evt);
            }
        });

        rechercherClient.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        rechercherClient.setText("Rechercher un client");
        rechercherClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rechercherClientActionPerformed(evt);
            }
        });

        afficherInfoClient.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        afficherInfoClient.setText("Afficher la liste de compte d'un client");
        afficherInfoClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                afficherInfoClientActionPerformed(evt);
            }
        });

        imprimerInfoClient.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        imprimerInfoClient.setText("Imprimer infos client");
        imprimerInfoClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimerInfoClientActionPerformed(evt);
            }
        });

        jButton9.setBackground(new java.awt.Color(102, 153, 255));
        jButton9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton9.setForeground(new java.awt.Color(255, 51, 102));
        jButton9.setText("Quitter");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        modifierInfoClient.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        modifierInfoClient.setText("Modifier infos client");
        modifierInfoClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierInfoClientActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(140, 140, 140)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(bouttonCreerAgence, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bouttonCreerClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bouttonCreerCompteBancaire, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rechercherCompte, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rechercherClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(afficherInfoClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(imprimerInfoClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(modifierInfoClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(113, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton9)
                .addGap(247, 247, 247))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(bouttonCreerAgence)
                .addGap(29, 29, 29)
                .addComponent(bouttonCreerClient)
                .addGap(26, 26, 26)
                .addComponent(bouttonCreerCompteBancaire)
                .addGap(18, 18, 18)
                .addComponent(rechercherCompte)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rechercherClient)
                .addGap(18, 18, 18)
                .addComponent(afficherInfoClient)
                .addGap(18, 18, 18)
                .addComponent(imprimerInfoClient)
                .addGap(18, 18, 18)
                .addComponent(modifierInfoClient)
                .addGap(18, 18, 18)
                .addComponent(jButton9)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 140, 620, 510));

        jPanel2.setBackground(new java.awt.Color(0, 204, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("Banque CDA");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(271, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(237, 237, 237))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(41, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(39, 39, 39))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 750, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
        InterfaceLogin login = new InterfaceLogin();
        login.setVisible(true);
        
    }//GEN-LAST:event_jButton9ActionPerformed

    private void bouttonCreerAgenceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bouttonCreerAgenceMouseClicked
        // TODO add your handling code here:
        JOptionPane.showConfirmDialog(this,
             "Vous allez creer une agence",
             "Connexion",
             
             JOptionPane.OK_CANCEL_OPTION);
        
        InterfaceCreationAgence creationAgence = new InterfaceCreationAgence();
        creationAgence.setVisible(true);
    }//GEN-LAST:event_bouttonCreerAgenceMouseClicked

    private void bouttonCreerClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bouttonCreerClientActionPerformed
        // TODO add your handling code here:
        
        JOptionPane.showConfirmDialog(this,
             "Vous allez creer un client",
             "Connexion",
             
             JOptionPane.OK_CANCEL_OPTION);
        
        InterfaceCreationClient creationClient = new InterfaceCreationClient();
        creationClient.setVisible(true);
    }//GEN-LAST:event_bouttonCreerClientActionPerformed

    private void rechercherCompteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rechercherCompteActionPerformed
        // TODO add your handling code here:
        
        JOptionPane.showConfirmDialog(this,
             "Vous allez rechercher un compte bancaire",
             "Connexion",
             
             JOptionPane.OK_CANCEL_OPTION);
        
        InterfaceRechercheCompte rechercherCompte = new InterfaceRechercheCompte();
        rechercherCompte.setVisible(true);
    }//GEN-LAST:event_rechercherCompteActionPerformed

    private void bouttonCreerCompteBancaireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bouttonCreerCompteBancaireActionPerformed
        // TODO add your handling code here:
        
        JOptionPane.showConfirmDialog(this,
             "Vous allez creer un compte bancaire",
             "Connexion",
             
             JOptionPane.OK_CANCEL_OPTION);
        
        InterfaceCreationCompteBancaire creationCompte = new InterfaceCreationCompteBancaire();
        creationCompte.setVisible(true);
    }//GEN-LAST:event_bouttonCreerCompteBancaireActionPerformed

    private void rechercherClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rechercherClientActionPerformed
        // TODO add your handling code here:
        JOptionPane.showConfirmDialog(this,
             "Vous allez rechercher un client",
             "Connexion",
             
             JOptionPane.OK_CANCEL_OPTION);
        InterfaceRechercheClient rechercheClient = new InterfaceRechercheClient();
        rechercheClient.setVisible(true);
        
    }//GEN-LAST:event_rechercherClientActionPerformed

    private void afficherInfoClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_afficherInfoClientActionPerformed
        // TODO add your handling code here:
        
         JOptionPane.showConfirmDialog(this,
             "Vous allez afficher la liste de compte d'un client",
             "Connexion",
             
             JOptionPane.OK_CANCEL_OPTION);
        InterfaceRechercheClient rechercheClient = new InterfaceRechercheClient();
        rechercheClient.setVisible(true);
    }//GEN-LAST:event_afficherInfoClientActionPerformed

    private void imprimerInfoClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imprimerInfoClientActionPerformed
        // TODO add your handling code here:
        
         JOptionPane.showConfirmDialog(this,
             "Vous allez imprimer les informations d'un client",
             "Connexion",
             
             JOptionPane.OK_CANCEL_OPTION);
        InterfaceImprimerInfosClient imprimerClient = new InterfaceImprimerInfosClient();
        imprimerClient.setVisible(true);
    }//GEN-LAST:event_imprimerInfoClientActionPerformed

    private void modifierInfoClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierInfoClientActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_modifierInfoClientActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfaceMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfaceMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfaceMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfaceMenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InterfaceMenuPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton afficherInfoClient;
    private javax.swing.JButton bouttonCreerAgence;
    private javax.swing.JButton bouttonCreerClient;
    private javax.swing.JButton bouttonCreerCompteBancaire;
    private javax.swing.JButton imprimerInfoClient;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton modifierInfoClient;
    private javax.swing.JButton rechercherClient;
    private javax.swing.JButton rechercherCompte;
    // End of variables declaration//GEN-END:variables
}
