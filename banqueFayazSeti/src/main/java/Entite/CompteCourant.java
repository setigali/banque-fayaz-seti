/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

/**
 *
 * @author setia
 */

public class CompteCourant extends CompteBancaire {

	private static final String nom = "Compte courant";
	
	
	public CompteCourant() {
		super();
	}

	public CompteCourant(String numeroDeCompte, float solde, boolean decouvert) {
		super(numeroDeCompte,solde, decouvert, 25);
		
	}

	
	public static String getNom() {
		return nom;
	}

	
	@Override
	public String toString() {
		return "CompteCourant [nom="+nom+", toString()=" + super.toString() + "]";
	}

	
}
