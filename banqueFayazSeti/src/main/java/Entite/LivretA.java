/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

/**
 *
 * @author setia
 */

public class LivretA extends CompteBancaire implements Comparable {
	
	private static final String nom = "Livret A";
	private boolean compteBloque;

	public LivretA(String numeroDeCompte, float solde, boolean decouvert, float fraisDeTenueDeCompte,
			boolean compteBloque) {
		super(numeroDeCompte,solde, decouvert, fraisDeTenueDeCompte);
		this.compteBloque = compteBloque;
	}

	public LivretA() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean isCompteBloque() {
		return compteBloque;
	}

	public void setCompteBloque(boolean compteBloque) {
		this.compteBloque = compteBloque;
	}

	public static String getNom() {
		return nom;
	}

	@Override
	public boolean equals(Object obj) {
		LivretA livretACaste= ((LivretA)obj);
		this.compteBloque=(livretACaste.compteBloque);
		return super.equals(obj);
	}

	@Override
	public int compareTo(Object obj) {
	
		return super.compareTo(obj);
	}	

}

    

