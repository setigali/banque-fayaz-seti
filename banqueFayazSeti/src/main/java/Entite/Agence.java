/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author setia
 */


public class Agence implements Comparable {

	public static String codeAgence;
	private String nom;
	private String adresse;
    
	private Map<String, Conseiller> listeConseiller;
	
	
	
	
	public Agence() {
		super();
	}

	public Agence(String codeAgence, String nom, String adresse) {
		super();
		Agence.codeAgence = codeAgence;
		this.nom = nom;
		this.adresse = adresse;
		this.listeConseiller = new HashMap<String, Conseiller>();
	}

	
	public String getCodeAgence() {
		return codeAgence;
	}

	public void setCodeAgence(String codeAgence) {
		this.codeAgence = codeAgence;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Map<String, Conseiller> getListeConseiller() {
		return listeConseiller;
	}

	public void setListeConseiller(HashMap<String, Conseiller> listeConseiller) {
		this.listeConseiller = listeConseiller;
	}

	
	/**
	 * On compare les agences via leur code agence
	 */
	@Override
	public int compareTo(Object o) {
		return this.codeAgence.compareTo( ((Agence) o).codeAgence );
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean code = this.codeAgence.equals( ((Agence) obj).codeAgence );
		boolean nom = this.nom.equals( ((Agence) obj).nom );
		boolean adresse = this.adresse.equals( ((Agence) obj).adresse );
		boolean liste = this.listeConseiller.equals( ((Agence) obj).listeConseiller );
		return code && nom && adresse && liste;
	}


	@Override
	public String toString() {
		return "Agence [codeAgence=" + codeAgence + ", nom=" + nom + ", adresse=" + adresse + ", listeConseiller="
				+ listeConseiller + "]";
	}

	
}

    

