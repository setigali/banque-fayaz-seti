/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author setia
 */


public class Banque  {
	private Scanner in ;
	private Map<String, Agence>listeAgences;

	
	public Banque(Scanner in) {
		super();
		this.listeAgences = new HashMap<String, Agence>();
		this.in=in;
		
	}


	public Banque() {
		super();
	}


	public Map<String, Agence> getListeAgences() {
		return listeAgences;
	}


	public void setListeAgences(HashMap<String, Agence> listeAgences) {
		this.listeAgences = listeAgences;
	}
	


	public void setIn(Scanner scanner) {
		this.in = scanner;
	}
	
	public Scanner getIn() {
		return in;
	}



	@Override
	public String toString() {
		return "Banque [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	

}

    

