/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author setia
 */

public class CompteBancaire implements Comparable {

	private String numeroDeCompte;
	private float solde;
	private boolean decouvert;
	private float fraisDeTenueDeCompte;
	private List<Operation> listeDesOperations;

	public CompteBancaire(String numeroDeCompte, float solde, boolean decouvert, float fraisDeTenueDeCompte) {
		super();
		this.numeroDeCompte = numeroDeCompte;
		this.solde = solde;
		this.decouvert = decouvert;
		this.fraisDeTenueDeCompte = fraisDeTenueDeCompte;
		this.listeDesOperations = new ArrayList<Operation>();
	}

	public CompteBancaire() {
		super();
	}

	public String getNumeroDeCompte() {
		return numeroDeCompte;
	}

	public void setNumeroDeCompte(String numeroDeCompte) {
		this.numeroDeCompte = numeroDeCompte;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}

	public float getFraisDeTenueDeCompte() {
		return fraisDeTenueDeCompte;
	}

	public void setFraisDeTenueDeCompte(float fraisDeTenueDeCompte) {
		this.fraisDeTenueDeCompte = fraisDeTenueDeCompte;
	}

	public List<Operation> getListeDesOperations() {
		return listeDesOperations;
	}

	public void setListeDesOperations(ArrayList<Operation> listeDesOperations) {
		this.listeDesOperations = listeDesOperations;
	}

	@Override
	public String toString() {
		return "CompteBancaire [numeroDeCompte=" + numeroDeCompte + ", solde=" + solde + ", decouvert=" + decouvert
				+ ", fraisDeTenueDeCompte=" + fraisDeTenueDeCompte + "]";
	}

	@Override
	public boolean equals(Object obj) {
	
		CompteBancaire compteBancaireCaste = ((CompteBancaire) obj);
		boolean b = this.numeroDeCompte.equals(compteBancaireCaste.numeroDeCompte)
				&& this.solde == (compteBancaireCaste.solde) && this.decouvert == (compteBancaireCaste.decouvert)
				&& this.fraisDeTenueDeCompte == (compteBancaireCaste.fraisDeTenueDeCompte);
		return b;
	}

	@Override
	public int compareTo(Object obj) {
		CompteBancaire compteBancaireCaste = ((CompteBancaire) obj);
		int comparaisonCompte = this.numeroDeCompte.compareTo(compteBancaireCaste.numeroDeCompte);
		return comparaisonCompte;

	}

}
