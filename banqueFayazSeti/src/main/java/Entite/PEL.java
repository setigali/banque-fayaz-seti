/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

/**
 *
 * @author setia
 */

public class PEL extends CompteBancaire {

	private static final String nom = "Plan Epargne Logement";
	
	// Lorsque le compte est bloque, "bloque" est a true.
	private boolean bloque;
	
	
	public PEL() {
		super();
	}

	/*public PEL(String numeroDeCompte, float solde, boolean decouvert, boolean compteBloque) {
		super(numeroDeCompte, solde, decouvert, PELServices.calculFrais(solde
		this.bloque = compteBloque;
	}*/
	

	public static String getNom() {
		return nom;
	}

	public boolean isBloque() {
		return bloque;
	}

	public void setBloque(boolean bloque) {
		this.bloque = bloque;
	}
	

	@Override
	public String toString() {
		return "PEL [bloque=" + bloque + ", toString()=" + super.toString() + "]";
	}
	
}

   
