/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entite;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author setia
 */

public class Conseiller implements Comparable{
	
	private String idConseiller;
	private String nom;
	private String prenom;
	private String mail;
	private Map<String, Client> listeClients;
	
	
	public Conseiller() {
		super();
	}
	
	public Conseiller(String idConseiller, String nom, String prenom, String mail) {
		super();
		this.idConseiller = idConseiller;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.listeClients = new HashMap<String, Client>();
	}

	
	public String getIdConseiller() {
		return idConseiller;
	}

	public void setIdConseiller(String idConseiller) {
		this.idConseiller = idConseiller;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Map<String, Client> getListeClients() {
		return listeClients;
	}

	public void setListeClients(HashMap<String, Client> listeClients) {
		this.listeClients = listeClients;
	}

	
	/**
	 * Compare les instances de conseiller via leur identifiant.
	 */
	@Override
	public int compareTo(Object o) {
		return this.idConseiller.compareTo( ((Conseiller) o).idConseiller );
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean conseiller = this.idConseiller.equals( ((Conseiller) obj).idConseiller );
		boolean nom = this.nom.equals(((Conseiller) obj).nom );
		boolean prenom = this.prenom.equals( ((Conseiller) obj).prenom );
		boolean mail = this.mail.equals( ((Conseiller) obj).mail );
		boolean clients = this.listeClients.equals(( (Conseiller) obj).listeClients );
		return conseiller && nom && prenom && mail && clients;
	}


	@Override
	public String toString() {
		return "Conseiller [idConseiller=" + idConseiller + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail
				+ ", listeClients=" + listeClients + "]";
	}


	
	
	
	
}

    

