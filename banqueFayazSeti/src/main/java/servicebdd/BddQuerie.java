package servicebdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.swing.JOptionPane;

public class BddQuerie {
	public static Connection connectionBase(){
	        
	    	String driverName = "org.postgresql.Driver";
			try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			// Infos de connection
			String url = "jdbc:postgresql://localhost:5432/banque";
			Properties props = new Properties();
			props.setProperty("user", "fayaz-seti");
			props.setProperty("password", "fayaz-seti");
	    	
	       
	    	Connection conn = null;
			try {
				conn = DriverManager.getConnection(url, props);
				JOptionPane.showMessageDialog(null, "Connection reuissie");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null, "Connection échouée");
			}
			
			  
			return conn;
	           
	
	    }
	
	public void insertAgence(String codeAgence, String nomAgenceSaisi, String adresseAgenceSaisi) {
		Connection connexion = connectionBase();
		 try {
				PreparedStatement prep = connexion.prepareStatement("insert into agence values (?,?,?,?)");
				prep.setObject(1, codeAgence);
				prep.setObject(2, nomAgenceSaisi);
				prep.setObject(3,  adresseAgenceSaisi);
				prep.setObject(4, new Integer(1));
				prep.executeUpdate();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
	}
	
	public void insertClient(String idClient, String nomClientSaisi, String prenomClientSaisi, String dateSaisi, String codeAgence, String adresseClientSaisi) {
		Connection connexion = connectionBase();
		//String requeteCodeAgence = "select id_agence from agence where nom = '"+codeAgence+"'";
		 try {
	   			PreparedStatement prep = connexion.prepareStatement("insert into personne values (?,?,?,?,?,?,?)");
	   			prep.setObject(1, idClient);
	   			prep.setObject(2, nomClientSaisi);
	   			prep.setObject(3,  prenomClientSaisi);
	   			prep.setObject(4, java.sql.Date.valueOf(dateSaisi));
	   			prep.setObject(5, new Integer(3));
	   			prep.setObject(6, codeAgence);
	   			prep.setObject(7, adresseClientSaisi);
	   			prep.executeUpdate();
	   		} catch (SQLException e1) {
	   			// TODO Auto-generated catch block
	   			e1.printStackTrace();
	   		}
	}
	
	public void insertCompte(String num_compte, float solde, boolean decouvert, String id_client, int selected) {
		Connection connexion = connectionBase();
	     try {
	   			PreparedStatement prep = connexion.prepareStatement("insert into compte values (?,?,?,?,?)");
	   			prep.setObject(1, num_compte);
	   			prep.setObject(2, solde);
	   			prep.setObject(3,  true);
	   			prep.setObject(4, id_client);
	   			prep.setObject(5, selected);
	   			prep.executeUpdate();
	   		} catch (SQLException e1) {
	   			// TODO Auto-generated catch block
	   			e1.printStackTrace();
	   		}
	}
	
	public String[] affichageListeAgence() {
		Connection connexion = connectionBase();
		ArrayList<String> listeAgence = new ArrayList<String>();
		String requete = "SELECT nom FROM agence";
		
		ResultSet resultat = null;
		 
		PreparedStatement prep;
		try {
			prep = connexion.prepareStatement(requete);

			resultat = prep.executeQuery();
			
			while (resultat.next()) {
				
				listeAgence.add(resultat.getString("nom"));
			
			}
		return	listeAgence.toArray(new String[0]);
			
			
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	
	}
        public String getCodeAgence(String nomAgence){
            Connection connexion = connectionBase();
            String requete = "SELECT id_agence FROM agence where nom = ?";
            ResultSet resultat = null;
            String id_agence = "";
	  PreparedStatement prep;
            try {
			prep = connexion.prepareStatement(requete);
                        prep.setObject(1, nomAgence);
			resultat = prep.executeQuery();
			
			while (resultat.next()) {
                            id_agence = resultat.getString("id_agence");
			}
		
			return id_agence;
			
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;          
        }
        
        public String[] getInfosClient(String idClient){
             Connection connexion = connectionBase();
            String requete = "SELECT id_personne, nom, prenom, datenaissance,adresse FROM personne where id_personne = ?";
            ResultSet resultat = null;
            String id_client = "";
            String nom = "";
            String prenom = "";
            String dateNaissance = "";
            String adresse = "";
            String[] infosClient = new String[5];
	  PreparedStatement prep;
            try {
			prep = connexion.prepareStatement(requete);
                        prep.setObject(1, idClient);
			resultat = prep.executeQuery();
			
			while (resultat.next()) {
                            id_client = resultat.getString("id_personne");
                            nom = resultat.getString("nom");
                            prenom = resultat.getString("prenom");
                            Date dateN = resultat.getDate("datenaissance");
                            dateNaissance = dateN.toString();
                            adresse = resultat.getString("adresse");
		}
		infosClient[0] = id_client;
                infosClient[1] = nom;
                infosClient[2] = prenom;
                infosClient[3] = dateNaissance;
                infosClient[4] = adresse;
		
                return infosClient;
			
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;  
        }
}
